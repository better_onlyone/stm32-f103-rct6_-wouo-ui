#include "stm32f10x.h"                  // Device header

//LED1 - PA8
//LED2 - PD2

void ledInit()
{
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOD, ENABLE);
	
	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOD,&GPIO_InitStructure);
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8;
	GPIO_Init(GPIOA,&GPIO_InitStructure);
	
}

void ledOn(uint8_t pin)
{
	if(pin == 1)
	{
		GPIO_ResetBits(GPIOA, GPIO_Pin_8);
	}
	else if(pin == 2)
	{
		GPIO_ResetBits(GPIOD, GPIO_Pin_2);
	}
}

void ledOff(uint8_t pin)
{
	if(pin == 1)
	{
		GPIO_SetBits(GPIOA, GPIO_Pin_8);
	}
	else if(pin == 2)
	{
		GPIO_SetBits(GPIOD, GPIO_Pin_2);
	}
}	

void ledReverse(uint8_t pin)
{
	if(pin == 1)
	{
		GPIO_WriteBit(GPIOA, GPIO_Pin_8, (BitAction)(1- GPIO_ReadOutputDataBit(GPIOA, GPIO_Pin_8)));
	}
	else if(pin == 2)
	{
		GPIO_WriteBit(GPIOD, GPIO_Pin_2, (BitAction)(1- GPIO_ReadOutputDataBit(GPIOD, GPIO_Pin_2)));
	}
}
