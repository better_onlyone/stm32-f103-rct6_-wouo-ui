#ifndef __LED_H
#define __LED_H

void ledInit(void);
void ledOff(uint8_t pin);
void ledOn(uint8_t pin);
void ledReverse(uint8_t pin);

#endif
