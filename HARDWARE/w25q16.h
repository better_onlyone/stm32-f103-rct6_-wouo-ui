#ifndef __W25Q16_H
#define __W25Q16_H

#include "stm32f10x.h"                  // Device header

//CS - PA2
//SCK - PA5
//MISO - PA6
//MOSI - PA7

#define W25Q16_INS_WriteEnable					0x06
#define W25Q16_INS_ReadStatusRegister1			0x05
#define W25Q16_INS_WriteStatusRegister1			0x01
#define W25Q16_INS_PageProgram					0x02
#define W25Q16_INS_SectorErase					0x20
#define W25Q16_INS_ReadData						0x03

#define W25Q16_INS_JEDEC_ID						0x9F

#define W25Q16_INS_DUMMY						0xFF

void Spi_SW_W_CS(uint8_t BitVal);
void Spi_SW_W_SCK(uint8_t BitVal);
uint8_t Spi_SW_R_MISO(void);
void Spi_SW_W_MOSI(uint8_t data);
void Spi_SW_Init(void);
void Spi_SW_Start(void);
void Spi_SW_Stop(void);
uint8_t Spi_SW_SwapData(uint8_t SendData);
void W25Q16_Init(void);
void W25Q16_GetID(uint8_t *MID, uint16_t *DID);
void W25Q16_WriteEnable(void);
void W25Q16_WaitBusy(uint32_t timeout);
void W25Q16_PageProgram(uint32_t Addr, uint8_t *DataArr, uint16_t Count);
void W25Q16_SectorErase(uint32_t Addr);
void W25Q16_ReadData(uint32_t Addr, uint8_t **DataArr, uint32_t Count);

#endif
