#ifndef __OLED_H
#define __OLED_H

void OLED_I2C_Init(void);

void OLED_SDA_HIGH(void);
void OLED_SDA_LOW(void);
void OLED_SCL_HIGH(void);
void OLED_SCL_LOW(void);

#endif
