#include "stm32f10x.h"                  // Device header
#include "delay.h"

//WK_UP - PA0
//KEY1 - PC5
//KEY2 - PA15
void keyInit()
{
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOC, ENABLE);
	
	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_5;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOC, &GPIO_InitStructure);
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_15;
	GPIO_Init(GPIOA, &GPIO_InitStructure);
	
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPD;
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0;
	GPIO_Init(GPIOA, &GPIO_InitStructure);
	
}

uint8_t key_GetNum(void)
{
	uint8_t key = 0;
	if(GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_5) == 0)
	{
		Delay_ms(20);
		if(GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_5) == 0)
		{
			while(GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_5) == 0);
			key = 1;
		}
		return key;
	}
	if(GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_15) == 0)
	{
		Delay_ms(20);
		if(GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_15) == 0)
		{
			while(GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_15) == 0);
			key = 2;
		}
		return key;
	}
	else
	{
		return 0;
	}
}
