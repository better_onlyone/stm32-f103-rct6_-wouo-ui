#include "stm32f10x.h" // Device header
#include <stdbool.h>
#include "delay.h"
#include "OLED.h"
#include <string.h>
#include <math.h>
#include "w25q16.h"

/************************************* 屏幕驱动 *************************************/

#include "u8g2.h"

extern u8g2_t u8g2;
u8g2_uint_t tx, ty;

static void setCursor(u8g2_uint_t x, u8g2_uint_t y)
{
  tx = x;
  ty = y;
}

// 分辨率128*64，软件IIC接口
uint8_t u8g2_gpio_and_delay_stm32(U8X8_UNUSED u8x8_t *u8x8, U8X8_UNUSED uint8_t msg, U8X8_UNUSED uint8_t arg_int, U8X8_UNUSED void *arg_ptr)
{
  switch (msg)
  {
  case U8X8_MSG_DELAY_MILLI: // Function which implements a delay, arg_int contains the amount of ms
    Delay_ms(arg_int);
    break;

  case U8X8_MSG_DELAY_10MICRO: // Function which delays 10us
    Delay_us(10);
    break;

  case U8X8_MSG_DELAY_100NANO: // Function which delays 100ns
    __NOP();
    break;
  case U8X8_MSG_GPIO_I2C_CLOCK:
    if (arg_int)
      OLED_SCL_HIGH();
    else
      OLED_SCL_LOW();
    break;
  case U8X8_MSG_GPIO_I2C_DATA:
    if (arg_int)
      OLED_SDA_HIGH();
    else
      OLED_SDA_LOW();
    break;
  default:
    return 0; // A message was received which is not implemented, return 0 to indicate an error
  }
  return 1; // command processed successfully.
}

uint8_t u8x8_gpio_and_delay_hw(u8x8_t *u8x8, uint8_t msg, uint8_t arg_int, void *arg_ptr)
{
  switch (msg)
  {

  case U8X8_MSG_GPIO_AND_DELAY_INIT:
    break;

  case U8X8_MSG_DELAY_MILLI:
    Delay_ms(arg_int);
    break;

  case U8X8_MSG_GPIO_I2C_CLOCK:
    break;

  case U8X8_MSG_GPIO_I2C_DATA:
    break;

  default:
    return 0;
  }
  return 1; // command processed successfully.
}
uint8_t u8x8_byte_hw_i2c(u8x8_t *u8x8, uint8_t msg, uint8_t arg_int, void *arg_ptr)
{
  uint8_t *data = (uint8_t *)arg_ptr;

  switch (msg)
  {
  case U8X8_MSG_BYTE_SEND:
    while (arg_int-- > 0)
    {
      I2C_SendData(I2C1, *data++);
      while (!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_BYTE_TRANSMITTED))
        continue;
    }
    break;
  case U8X8_MSG_BYTE_INIT:
    /* add your custom code to init i2c subsystem */
	OLED_I2C_Init();
    break;
  case U8X8_MSG_BYTE_SET_DC:
    /* ignored for i2c */
    break;
  case U8X8_MSG_BYTE_START_TRANSFER:
    I2C_GenerateSTART(I2C1, ENABLE);
    while (I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_MODE_SELECT) != SUCCESS)
      ;
    I2C_Send7bitAddress(I2C1, 0x78, I2C_Direction_Transmitter);
    while (I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED) != SUCCESS)
      ;
    break;
  case U8X8_MSG_BYTE_END_TRANSFER:
    I2C_GenerateSTOP(I2C1, ENABLE);
    break;
  default:
    return 0;
  }
  return 1;
}

void u8g2_Init(void)
{
  // u8g2_Setup_ssd1306_i2c_128x64_noname_f(&u8g2, U8G2_R0, u8x8_byte_sw_i2c, u8g2_gpio_and_delay_stm32);

  u8g2_Setup_ssd1306_i2c_128x64_noname_f(&u8g2, U8G2_R0, u8x8_byte_hw_i2c, u8x8_gpio_and_delay_hw);
}

/************************************* 定义页面 *************************************/

// 总目录，缩进表示页面层级
enum
{
  M_WINDOW,
  M_SLEEP,
  M_MAIN,
  M_EDITOR,
  M_KNOB,
  M_KRF,
  M_KPF,
  M_SETTING,
  M_ABOUT,
};

// 状态，初始化标签
enum
{
  S_FADE,      // 转场动画
  S_WINDOW,    // 弹窗初始化
  S_LAYER_IN,  // 层级初始化
  S_LAYER_OUT, // 层级初始化
  S_NONE,      // 直接选择页面
};

// 菜单结构体
typedef struct MENU
{
  char *m_select;
} M_SELECT;

/************************************* 定义内容 *************************************/

M_SELECT main_menu[] =
    {
        {"[ Main Menu ]"},
        {"- Editor"},
        {"- Setting"},
};

M_SELECT editor_menu[] =
    {
        {"[ Editor ]"},
        {"- Function 0"},
        {"- Function 1"},
        {"- Function 2"},
        {"- Function 3"},
        {"- Function 4"},
        {"- Function 5"},
        {"- Function 6"},
        {"- Function 7"},
        {"- Function 8"},
        {"- Function 9"},
        {"- Knob"},
};

M_SELECT knob_menu[] =
    {
        {"[ Knob ]"},
        {"# Rotate Func"},
        {"$ Press Func"},
};

M_SELECT krf_menu[] =
    {
        {"[ Rotate Function ]"},
        {"--------------------------"},
        {"= Disable"},
        {"--------------------------"},
        {"= Volume"},
        {"= Brightness"},
        {"--------------------------"},
};

M_SELECT kpf_menu[] =
    {
        {"[ Press Function ]"},
        {"--------------------------"},
        {"= Disable"},
        {"--------------------------"},
        {"= A"},
        {"= B"},
        {"= C"},
        {"= D"},
        {"= E"},
        {"= F"},
        {"= G"},
        {"= H"},
        {"= I"},
        {"= J"},
        {"= K"},
        {"= L"},
        {"= M"},
        {"= N"},
        {"= O"},
        {"= P"},
        {"= Q"},
        {"= R"},
        {"= S"},
        {"= T"},
        {"= U"},
        {"= V"},
        {"= W"},
        {"= X"},
        {"= Y"},
        {"= Z"},
        {"--------------------------"},
        {"= 0"},
        {"= 1"},
        {"= 2"},
        {"= 3"},
        {"= 4"},
        {"= 5"},
        {"= 6"},
        {"= 7"},
        {"= 8"},
        {"= 9"},
        {"--------------------------"},
        {"= Esc"},
        {"= F1"},
        {"= F2"},
        {"= F3"},
        {"= F4"},
        {"= F5"},
        {"= F6"},
        {"= F7"},
        {"= F8"},
        {"= F9"},
        {"= F10"},
        {"= F11"},
        {"= F12"},
        {"--------------------------"},
        {"= Left Ctrl"},
        {"= Left Shift"},
        {"= Left Alt"},
        {"= Left Win"},
        {"= Right Ctrl"},
        {"= Right Shift"},
        {"= Right Alt"},
        {"= Right Win"},
        {"--------------------------"},
        {"= Caps Lock"},
        {"= Backspace"},
        {"= Return"},
        {"= Insert"},
        {"= Delete"},
        {"= Tab"},
        {"--------------------------"},
        {"= Home"},
        {"= End"},
        {"= Page Up"},
        {"= Page Down"},
        {"--------------------------"},
        {"= Up Arrow"},
        {"= Down Arrow"},
        {"= Left Arrow"},
        {"= Right Arrow"},
        {"--------------------------"},
};

M_SELECT setting_menu[] =
    {
        {"[ Setting ]"},
        {"~ Disp Bri"},
        {"~ List Ani"},
        {"~ Win Ani"},
        {"~ Fade Ani"},
        {"~ key_msg SPT"},
        {"~ key_msg LPT"},
        {"+ L Ufd Fm Scr"},
        {"+ L Loop Mode"},
        {"+ Win Bokeh Bg"},
        {"+ Knob Rot Dir"},
        {"+ Dark Mode"},
        {"- [ About ]"},
};

M_SELECT about_menu[] =
    {
        {"[ WouoUI ]"},
        {"- Version: v0.1"},
        {"- Board: STM32F103RCT6"},
        {"- Ram: 20k"},
        {"- Flash: 64k"},
        {"- Freq: 72Mhz"},
        {"- NO USB"},
        {"- NO EEPROM"},
};

/************************************* 页面变量 *************************************/

// OLED变量
#define DISP_H 64  // 屏幕高度
#define DISP_W 128 // 屏幕宽度
uint8_t *buf_ptr;  // 指向屏幕缓冲的指针
uint16_t buf_len;  // 缓冲长度

// UI变量
#define UI_DEPTH 20  // 最深层级数
#define UI_MNUMB 100 // 菜单数量
#define UI_PARAM 11  // 参数数量
enum
{
  DISP_BRI,  // 屏幕亮度
  LIST_ANI,  // 列表动画速度
  WIN_ANI,   // 弹窗动画速度
  FADE_ANI,  // 消失动画速度
  BTN_SPT,   // 按键短按时长
  BTN_LPT,   // 按键长按时长
  LIST_UFD,  // 菜单列表从头展开开关
  LIST_LOOP, // 菜单列表循环模式开关
  WIN_BOK,   // 弹窗背景虚化开关
  KNOB_DIR,  // 旋钮方向切换开关
  DARK_MODE, // 黑暗模式开关
};
struct
{
  bool init;
  uint8_t num[UI_MNUMB];
  uint8_t select[UI_DEPTH];
  uint8_t layer;
  uint8_t index = M_SLEEP;
  uint8_t state = S_NONE;
  bool sleep = true;
  uint8_t fade = 1;
  uint8_t param[UI_PARAM];
} ui;

// 列表变量
// 默认参数

#define   LIST_FONT           u8g2_font_HelvetiPixel_tr   //列表字体
#define   LIST_TEXT_H         8                           //列表每行文字字体的高度
#define   LIST_LINE_H         16                          //列表单行高度
#define   LIST_TEXT_S         4                           //列表每行文字的上边距，左边距和右边距，下边距由它和字体高度和行高度决定
#define   LIST_BAR_W          5                           //列表进度条宽度，需要是奇数，因为正中间有1像素宽度的线
#define   LIST_BOX_R          0.5                         //列表选择框圆角


//// 超窄行高度测试
//#define LIST_FONT u8g2_font_4x6_tr // 列表字体
//#define LIST_TEXT_H 5              // 列表每行文字字体的高度
//#define LIST_LINE_H 7              // 列表单行高度
//#define LIST_TEXT_S 1              // 列表每行文字的上边距，左边距和右边距，下边距由它和字体高度和行高度决定
//#define LIST_BAR_W 7               // 列表进度条宽度，需要是奇数，因为正中间有1像素宽度的线
//#define LIST_BOX_R 0.5             // 列表选择框圆角

struct
{
  uint8_t line_n = DISP_H / LIST_LINE_H;
  int16_t temp;
  bool loop;
  float y;
  float y_trg;
  float box_x;
  float box_x_trg;
  float box_y;
  float box_y_trg[UI_DEPTH];
  float bar_y;
  float bar_y_trg;
} list;

// 选择框变量

//默认参数
#define   CHECK_BOX_L_S       95                          //选择框在每行的左边距
#define   CHECK_BOX_U_S       2                           //选择框在每行的上边距
#define   CHECK_BOX_F_W       12                          //选择框外框宽度
#define   CHECK_BOX_F_H       12                          //选择框外框高度
#define   CHECK_BOX_D_S       2                           //选择框里面的点距离外框的边距

// 超窄行高度测试
//#define CHECK_BOX_L_S 99 // 选择框在每行的左边距
//#define CHECK_BOX_U_S 0  // 选择框在每行的上边距
//#define CHECK_BOX_F_W 5  // 选择框外框宽度
//#define CHECK_BOX_F_H 5  // 选择框外框高度
//#define CHECK_BOX_D_S 1  // 选择框里面的点距离外框的边距

struct
{
  uint8_t *v;
  uint8_t *m;
  uint8_t *s;
  uint8_t *s_p;
} check_box;

// 弹窗变量
#define WIN_FONT u8g2_font_HelvetiPixel_tr // 弹窗字体
#define WIN_H 32                           // 弹窗高度
#define WIN_W 102                          // 弹窗宽度
#define WIN_BAR_W 92                       // 弹窗进度条宽度
#define WIN_BAR_H 7                        // 弹窗进度条高度
#define WIN_Y -WIN_H - 2                   // 弹窗竖直方向出场起始位置
#define WIN_Y_TRG -WIN_H - 2               // 弹窗竖直方向退场终止位置
struct
{
  // uint8_t
  uint8_t *value;
  uint8_t max;
  uint8_t min;
  uint8_t step;

  MENU *bg;
  uint8_t index;
  char title[20];
  uint8_t select;
  uint8_t l = (DISP_W - WIN_W) / 2;
  uint8_t u = (DISP_H - WIN_H) / 2;
  float bar;
  float bar_trg;
  float y;
  float y_trg;
} win;

/************************************* USB 相关 *************************************/

/********************************** 自定义功能变量 **********************************/

// 旋钮功能变量
#define KNOB_PARAM 4
#define KNOB_DISABLE 0
#define KNOB_ROT_VOL 1
#define KNOB_ROT_BRI 2
enum
{
  KNOB_ROT,   // 睡眠下旋转旋钮的功能，0 禁用，1 音量，2 亮度
  KNOB_COD,   // 睡眠下短按旋钮输入的字符码，0 禁用
  KNOB_ROT_P, // 旋转旋钮功能在单选框中选择的位置
  KNOB_COD_P, // 字符码在单选框中选择的位置
};
struct
{
  uint8_t param[KNOB_PARAM] = {KNOB_DISABLE, KNOB_DISABLE, 2, 2}; // 禁用在列表的第2个选项，第0个是标题，第1个是分界线
} knob;

/************************************* 断电保存 *************************************/
// EEPROM变量
#define EEPROM_ADDRESS 0x000000
#define EEPROM_CHECK 11

void ui_param_init(void);

struct
{
  bool init;
  bool change;
  uint32_t address;
  uint8_t check;
  uint8_t check_param[EEPROM_CHECK] = {'z', 'b', 'c', 'b', 'e', 'f', 'g', 'h', 'i', 'j', 'k'};
} eeprom;
// EEPROM写数据，回到睡眠时执行一遍
#define DEBUG 1
void eeprom_write_all_data()
{
	uint8_t i;
    uint8_t send_buf[100];
#if DEBUG	
	uint8_t test_buf[100];		//这些代码是DEBUG时遗留的，release应该要删掉
	uint8_t *ptr = test_buf;	//但是删掉之后断电保存就不能用了,所以请不要修改这部分代码
#endif
	eeprom.address = EEPROM_ADDRESS;
	for(i = 0; 	i < EEPROM_CHECK;	i++) send_buf[i] = eeprom.check_param[i]; 
	for(i = 0;	i < UI_PARAM; 		i++) send_buf[i+EEPROM_CHECK] = ui.param[i]; 
	for(i = 0; 	i < KNOB_PARAM;		i++) send_buf[i+EEPROM_CHECK+UI_PARAM] = knob.param[i]; 
	
    W25Q16_SectorErase(eeprom.address);	//必须擦除再写入
#if DEBUG
	W25Q16_ReadData(eeprom.address, &ptr, EEPROM_CHECK+UI_PARAM+KNOB_PARAM);	//DEBUG遗留,同上
#endif
	W25Q16_PageProgram(eeprom.address, send_buf, EEPROM_CHECK+UI_PARAM+KNOB_PARAM);
#if DEBUG
	W25Q16_ReadData(eeprom.address, &ptr, EEPROM_CHECK+UI_PARAM+KNOB_PARAM);	//DEBUG遗留，同上
#endif
}

// EEPROM读数据，开机初始化时执行一遍

void eeprom_read_all_data()
{
	uint8_t *ui_ptr = ui.param;
	uint8_t *knob_ptr = knob.param;
  eeprom.address = EEPROM_ADDRESS + EEPROM_CHECK;
  W25Q16_ReadData(eeprom.address, &ui_ptr, UI_PARAM);
  eeprom.address += UI_PARAM;
  W25Q16_ReadData(eeprom.address, &knob_ptr, KNOB_PARAM);
  eeprom.address += KNOB_PARAM;
}

// 开机检查是否已经修改过，没修改过则跳过读配置步骤，用默认设置
void eeprom_init()
{
  uint8_t check_wrong[EEPROM_CHECK] = {0};
  uint8_t *ptr = check_wrong;
  eeprom.check = 0;
  eeprom.address = EEPROM_ADDRESS;
  W25Q16_ReadData(eeprom.address, &ptr, EEPROM_CHECK);
  for (uint8_t i = 0; i < EEPROM_CHECK; i++)
    if (check_wrong[i] != eeprom.check_param[i])
      eeprom.check++;
  if (eeprom.check <= 1)
    eeprom_read_all_data(); // 允许一位误码
  else
    ui_param_init();
}

/************************************* 按键相关 *************************************/

#define BTN_UP 0
#define BTN_DOWN 1
#define BTN_LEFT 2
#define BTN_RIGHT 3

typedef struct
{
  uint8_t val;
  uint8_t last_val;
} KEY_T;

typedef struct
{
  uint8_t id;
  uint8_t press;
  uint8_t update_flag;
  uint8_t res;
} KEY_MSG;

KEY_T key[4] = {0};
KEY_MSG key_msg = {0};

static uint8_t get_btn_val(uint8_t pin)
{
  uint8_t val = 0;
  switch (pin)
  {
  case 0:
    val = GPIO_ReadInputDataBit(GPIOB, GPIO_Pin_11);
    break;
  case 1:
    val = GPIO_ReadInputDataBit(GPIOB, GPIO_Pin_12);
    break;
  case 2:
    val = GPIO_ReadInputDataBit(GPIOB, GPIO_Pin_13);
    break;
  case 3:
    val = GPIO_ReadInputDataBit(GPIOB, GPIO_Pin_14);
    break;
  }
  return val;
}

void key_init(void)
{
  GPIO_InitTypeDef GPIO_IniyStructure;
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
  GPIO_IniyStructure.GPIO_Mode = GPIO_Mode_IPU;
  GPIO_IniyStructure.GPIO_Pin = GPIO_Pin_11 | GPIO_Pin_12 | GPIO_Pin_13 | GPIO_Pin_14;
  GPIO_IniyStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_Init(GPIOB, &GPIO_IniyStructure);
  for (int i = 0; i < 4; i++)
  {
    key[i].val = key[i].last_val = get_btn_val(i);
  }
}

void key_scan(void)
{
  for (int i = 0; i < 4; i++)
  {
    key[i].val = get_btn_val(i);
    if (key[i].val != key[i].last_val)
    {
      key[i].last_val = key[i].val;
      if (key[i].val == 0)
      {
        key_msg.id = i;
        key_msg.press = 1;
        key_msg.update_flag = 1;
      }
    }
  }
}

/************************************* 旋钮相关 *************************************/

/************************************ 初始化函数 ***********************************/

/********************************* 初始化数据处理函数 *******************************/

// 显示数值的初始化
void check_box_v_init(uint8_t *param)
{
  check_box.v = param;
}

// 多选框的初始化
void check_box_m_init(uint8_t *param)
{
  check_box.m = param;
}

// 单选框时的初始化
void check_box_s_init(uint8_t *param, uint8_t *param_p)
{
  check_box.s = param;
  check_box.s_p = param_p;
}

// 多选框处理函数
void check_box_m_select(uint8_t param)
{
  check_box.m[param] = !check_box.m[param];
  eeprom.change = true;
}

// 单选框处理函数
void check_box_s_select(uint8_t val, uint8_t pos)
{
  *check_box.s = val;
  *check_box.s_p = pos;
  eeprom.change = true;
}

// 弹窗数值初始化
void window_value_init(char title[], uint8_t select, uint8_t *value, uint8_t max, uint8_t min, uint8_t step, MENU *bg, uint8_t index)
{
  strcpy(win.title, title);
  win.select = select;
  win.value = value;
  win.max = max;
  win.min = min;
  win.step = step;
  win.bg = bg;
  win.index = index;
  ui.index = M_WINDOW;
  ui.state = S_WINDOW;
}

/*********************************** UI 初始化函数 *********************************/

// 在初始化EEPROM时，选择性初始化的默认设置
void ui_param_init()
{
  ui.param[DISP_BRI] = 255; // 屏幕亮度
  ui.param[LIST_ANI] = 15;  // 列表动画速度
  ui.param[WIN_ANI] = 10;   // 弹窗动画速度
  ui.param[FADE_ANI] = 1;   // 消失动画速度
  ui.param[BTN_SPT] = 25;   // 按键短按时长
  ui.param[BTN_LPT] = 150;  // 按键长按时长
  ui.param[LIST_UFD] = 1;   // 菜单列表从头展开开关
  ui.param[LIST_LOOP] = 1;  // 菜单列表循环模式开关
  ui.param[WIN_BOK] = 0;    // 弹窗背景虚化开关
  ui.param[KNOB_DIR] = 0;   // 旋钮方向切换开关
  ui.param[DARK_MODE] = 1;  // 黑暗模式开关
}

// 列表类页面列表行数初始化，必须初始化的参数
void ui_init()
{
  ui.num[M_MAIN] = sizeof(main_menu) / sizeof(M_SELECT);
  ui.num[M_EDITOR] = sizeof(editor_menu) / sizeof(M_SELECT);
  ui.num[M_KNOB] = sizeof(knob_menu) / sizeof(M_SELECT);
  ui.num[M_KRF] = sizeof(krf_menu) / sizeof(M_SELECT);
  ui.num[M_KPF] = sizeof(kpf_menu) / sizeof(M_SELECT);
  ui.num[M_SETTING] = sizeof(setting_menu) / sizeof(M_SELECT);
  ui.num[M_ABOUT] = sizeof(about_menu) / sizeof(M_SELECT);
}

/********************************* 分页面初始化函数 ********************************/

// 进入睡眠时的初始化
void sleep_param_init()
{
  u8g2_SetDrawColor(&u8g2, 0);
  u8g2_DrawBox(&u8g2, 0, 0, DISP_W, DISP_H);
  u8g2_SetPowerSave(&u8g2, 1);
  ui.state = S_NONE;
  ui.sleep = true;
  if (eeprom.change)
  {
    eeprom_write_all_data();
    eeprom.change = false;
  }
}

// 旋钮设置页初始化
void knob_param_init() { check_box_v_init(knob.param); }

// 旋钮旋转页初始化
void krf_param_init() { check_box_s_init(&knob.param[KNOB_ROT], &knob.param[KNOB_ROT_P]); }

// 旋钮点按页初始化
void kpf_param_init() { check_box_s_init(&knob.param[KNOB_COD], &knob.param[KNOB_COD_P]); }

// 设置页初始化
void setting_param_init()
{
  check_box_v_init(ui.param);
  check_box_m_init(ui.param);
}

/********************************** 通用初始化函数 *********************************/

/*
  页面层级管理逻辑是，把所有页面都先当作列表类初始化，不是列表类按需求再初始化对应函数
  这样做会浪费一些资源，但跳转页面时只需要考虑页面层级，逻辑上更清晰，减少出错
*/

// 弹窗动画初始化
void window_param_init()
{
  win.bar = 0;
  win.y = WIN_Y;
  win.y_trg = win.u;
  ui.state = S_NONE;
}

// 进入更深层级时的初始化
void layer_init_in()
{
  ui.layer++;
  ui.init = 0;
  list.y = 0;
  list.y_trg = LIST_LINE_H;
  list.box_x = 0;
  list.box_y = 0;
  list.bar_y = 0;
  ui.state = S_FADE;
  switch (ui.index)
  {
  case M_KNOB:
    knob_param_init();
    break; // 旋钮设置页，行末尾文字初始化
  case M_KRF:
    krf_param_init();
    break; // 旋钮旋转页，单选框初始化
  case M_KPF:
    kpf_param_init();
    break; // 旋钮点按页，单选框初始化
  case M_SETTING:
    setting_param_init();
    break; // 主菜单进入设置页，单选框初始化
  }
}

// 进入更浅层级时的初始化
void layer_init_out()
{
  ui.select[ui.layer] = 0;
  list.box_y_trg[ui.layer] = 0;
  ui.layer--;
  ui.init = 0;
  list.y = 0;
  list.y_trg = LIST_LINE_H;
  list.bar_y = 0;
  ui.state = S_FADE;
  switch (ui.index)
  {
  case M_SLEEP:
    sleep_param_init();
    break; // 主菜单进入睡眠页，检查是否需要写EEPROM
  }
}

/************************************* 动画函数 *************************************/

// 动画函数
void animation(float *a, float *a_trg, uint8_t n)
{
  if (fabs(*a - *a_trg) < 0.15)
    *a = *a_trg;
  if (*a != *a_trg)
    *a += (*a_trg - *a) / (ui.param[n] / 10.0);
}

// 消失函数
void fade()
{
  Delay_ms(ui.param[FADE_ANI]);
  if (ui.param[DARK_MODE])
  {
    switch (ui.fade)
    {
    case 1:
      for (uint16_t i = 0; i < buf_len; ++i)
        if (i % 2 != 0)
          buf_ptr[i] = buf_ptr[i] & 0xAA;
      break;
    case 2:
      for (uint16_t i = 0; i < buf_len; ++i)
        if (i % 2 != 0)
          buf_ptr[i] = buf_ptr[i] & 0x00;
      break;
    case 3:
      for (uint16_t i = 0; i < buf_len; ++i)
        if (i % 2 == 0)
          buf_ptr[i] = buf_ptr[i] & 0x55;
      break;
    case 4:
      for (uint16_t i = 0; i < buf_len; ++i)
        if (i % 2 == 0)
          buf_ptr[i] = buf_ptr[i] & 0x00;
      break;
    default:
      ui.state = S_NONE;
      ui.fade = 0;
      break;
    }
  }
  else
  {
    switch (ui.fade)
    {
    case 1:
      for (uint16_t i = 0; i < buf_len; ++i)
        if (i % 2 != 0)
          buf_ptr[i] = buf_ptr[i] | 0xAA;
      break;
    case 2:
      for (uint16_t i = 0; i < buf_len; ++i)
        if (i % 2 != 0)
          buf_ptr[i] = buf_ptr[i] | 0x00;
      break;
    case 3:
      for (uint16_t i = 0; i < buf_len; ++i)
        if (i % 2 == 0)
          buf_ptr[i] = buf_ptr[i] | 0x55;
      break;
    case 4:
      for (uint16_t i = 0; i < buf_len; ++i)
        if (i % 2 == 0)
          buf_ptr[i] = buf_ptr[i] | 0x00;
      break;
    default:
      ui.state = S_NONE;
      ui.fade = 0;
      break;
    }
  }
  ui.fade++;
}

/************************************* 显示函数 *************************************/
static char *itoa(uint8_t num)
{
  char index[] = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"; // 索引表
														// num存放要转换的整数
  int i = 0, j, k = 0;                                   // i用来指示设置字符串相应位，转换之后i其实就是字符串的长度；转换后顺序是逆序的，有正负的情况，k用来指示调整顺序的开始位置;j用来指示调整顺序时的交换。
  static char str[10] = {0};
  // 获取要转换的整数的绝对值

  // 转换部分，注意转换后是逆序的
  do
  {
    str[i++] = index[num % 10]; // 取unum的最后一位，并设置为str对应位，指示索引加1
    num /= 10;                  // unum去掉最后一位

  } while (num); // 直至unum为0退出循环

  str[i] = '\0'; // 在字符串最后添加'\0'字符，c语言字符串以'\0'结束。

  // 将顺序调整过来

  char temp;                         // 临时变量，交换两个值时用到
  for (j = k; j <= (i - 1) / 2; j++) // 头尾一一对称交换，i其实就是字符串的长度，索引最大值比长度少1
  {
    temp = str[j];               // 头部赋值给临时变量
    str[j] = str[i - 1 + k - j]; // 尾部赋值给头部
    str[i - 1 + k - j] = temp;   // 将临时变量的值(其实就是之前的头部值)赋给尾部
  }
  return str; // 返回转换后的字符串
}
/*************** 根据列表每行开头符号，判断每行尾部是否绘制以及绘制什么内容 *************/

// 列表显示数值
void list_draw_value(int n)
{
  u8g2_DrawStr(&u8g2, tx, ty, itoa(check_box.v[n - 1]));
}
// 绘制外框
void list_draw_check_box_frame() { u8g2_DrawRFrame(&u8g2, CHECK_BOX_L_S, list.temp + CHECK_BOX_U_S, CHECK_BOX_F_W, CHECK_BOX_F_H, 1); }

// 绘制框里面的点
void list_draw_check_box_dot() { u8g2_DrawBox(&u8g2, CHECK_BOX_L_S + CHECK_BOX_D_S + 1, list.temp + CHECK_BOX_U_S + CHECK_BOX_D_S + 1, CHECK_BOX_F_W - (CHECK_BOX_D_S + 1) * 2, CHECK_BOX_F_H - (CHECK_BOX_D_S + 1) * 2); }

// 列表显示旋钮功能
void list_draw_krf(int n)
{
  switch (check_box.v[n - 1])
  {
  case 0:
    u8g2_DrawStr(&u8g2, tx, ty, "OFF");
    break;
  case 1:
    u8g2_DrawStr(&u8g2, tx, ty, "VOL");
    break;
  case 2:
    u8g2_DrawStr(&u8g2, tx, ty, "BRI");
    break;
  }
}

// 列表显示按键键值
void list_draw_kpf(int n)
{
  if (check_box.v[n - 1] == 0)
    u8g2_DrawStr(&u8g2, tx, ty, "OFF");
  else if (check_box.v[n - 1] <= 90)
    u8g2_DrawStr(&u8g2, tx, ty, itoa(check_box.v[n - 1]));
  else
    u8g2_DrawStr(&u8g2, tx, ty, "?");
}

// 判断列表尾部内容
void list_draw_text_and_check_box(struct MENU arr[], int i)
{
  u8g2_DrawStr(&u8g2, LIST_TEXT_S, list.temp + LIST_TEXT_H + LIST_TEXT_S, arr[i].m_select);
  setCursor(CHECK_BOX_L_S, list.temp + LIST_TEXT_H + LIST_TEXT_S);
  switch (arr[i].m_select[0])
  {
  case '~':
    list_draw_value(i);
    break;
  case '+':
    list_draw_check_box_frame();
    if (check_box.m[i - 1] == 1)
      list_draw_check_box_dot();
    break;
  case '=':
    list_draw_check_box_frame();
    if (*check_box.s_p == i)
      list_draw_check_box_dot();
    break;
  case '#':
    list_draw_krf(i);
    break;
  case '$':
    list_draw_kpf(i);
    break;
  }
}

/******************************** 列表显示函数 **************************************/

// 列表类页面通用显示函数
void list_show(struct MENU arr[], uint8_t ui_index)
{
  // 更新动画目标值
  u8g2_SetFont(&u8g2, LIST_FONT);
  list.box_x_trg = u8g2_GetStrWidth(&u8g2, arr[ui.select[ui.layer]].m_select) + LIST_TEXT_S * 2;
  list.bar_y_trg = ceil((ui.select[ui.layer]) * ((float)DISP_H / (ui.num[ui_index] - 1)));

  // 计算动画过渡值
  animation(&list.y, &list.y_trg, LIST_ANI);
  animation(&list.box_x, &list.box_x_trg, LIST_ANI);
  animation(&list.box_y, &list.box_y_trg[ui.layer], LIST_ANI);
  animation(&list.bar_y, &list.bar_y_trg, LIST_ANI);

  // 检查循环动画是否结束
  if (list.loop && list.box_y == list.box_y_trg[ui.layer])
    list.loop = false;

  // 设置文字和进度条颜色，0透显，1实显，2反色，这里都用实显
  u8g2_SetDrawColor(&u8g2, 1);

  // 绘制进度条
  u8g2_DrawHLine(&u8g2, DISP_W - LIST_BAR_W, 0, LIST_BAR_W);
  u8g2_DrawHLine(&u8g2, DISP_W - LIST_BAR_W, DISP_H - 1, LIST_BAR_W);
  u8g2_DrawVLine(&u8g2, DISP_W - ceil((float)LIST_BAR_W / 2), 0, DISP_H);
  u8g2_DrawBox(&u8g2, DISP_W - LIST_BAR_W, 0, LIST_BAR_W, list.bar_y);

  // 绘制列表文字
  if (!ui.init)
  {
    for (int i = 0; i < ui.num[ui_index]; ++i)
    {
      if (ui.param[LIST_UFD])
        list.temp = i * list.y - LIST_LINE_H * ui.select[ui.layer] + list.box_y_trg[ui.layer];
      else
        list.temp = (i - ui.select[ui.layer]) * list.y + list.box_y_trg[ui.layer];
      list_draw_text_and_check_box(arr, i);
    }
    if (list.y == list.y_trg)
    {
      ui.init = true;
      list.y = list.y_trg = -LIST_LINE_H * ui.select[ui.layer] + list.box_y_trg[ui.layer];
    }
  }
  else
    for (int i = 0; i < ui.num[ui_index]; ++i)
    {
      list.temp = LIST_LINE_H * i + list.y;
      list_draw_text_and_check_box(arr, i);
    }

  // 绘制文字选择框，0透显，1实显，2反色，这里用反色
  u8g2_SetDrawColor(&u8g2, 2);
  u8g2_DrawRBox(&u8g2, 0, list.box_y, list.box_x, LIST_LINE_H, LIST_BOX_R);

  // 反转屏幕内元素颜色，白天模式遮罩，在这里屏蔽有列表参与的页面，使遮罩作用在那个页面上
  if (!ui.param[DARK_MODE])
  {
    u8g2_DrawBox(&u8g2, 0, 0, DISP_W, DISP_H);
    switch (ui.index)
    {
    case M_WINDOW:
      u8g2_DrawBox(&u8g2, 0, 0, DISP_W, DISP_H);
    }
  }
}

// 弹窗通用显示函数
void window_show()
{
  // 绘制背景列表，根据开关判断背景是否要虚化
  list_show(win.bg, win.index);
  if (ui.param[WIN_BOK])
    for (uint16_t i = 0; i < buf_len; ++i)
      buf_ptr[i] = buf_ptr[i] & (i % 2 == 0 ? 0x55 : 0xAA);

  // 更新动画目标值
  u8g2_SetFont(&u8g2, WIN_FONT);
  win.bar_trg = (float)(*win.value - win.min) / (float)(win.max - win.min) * (WIN_BAR_W - 4);

  // 计算动画过渡值
  animation(&win.bar, &win.bar_trg, WIN_ANI);
  animation(&win.y, &win.y_trg, WIN_ANI);

  // 绘制窗口
  u8g2_SetDrawColor(&u8g2, 0);
  u8g2_DrawRBox(&u8g2, win.l, (int16_t)win.y, WIN_W, WIN_H, 2); // 绘制外框背景
  u8g2_SetDrawColor(&u8g2, 1);
  u8g2_DrawRFrame(&u8g2, win.l, (int16_t)win.y, WIN_W, WIN_H, 2);                  // 绘制外框描边
  u8g2_DrawRFrame(&u8g2, win.l + 5, (int16_t)win.y + 20, WIN_BAR_W, WIN_BAR_H, 1); // 绘制进度条外框
  u8g2_DrawBox(&u8g2, win.l + 7, (int16_t)win.y + 22, win.bar, WIN_BAR_H - 4);     // 绘制进度条
  setCursor(win.l + 5, (int16_t)win.y + 14);
  u8g2_DrawStr(&u8g2, tx, ty, win.title); // 绘制标题
  setCursor(win.l + 78, (int16_t)win.y + 14);
  u8g2_DrawStr(&u8g2, tx, ty, itoa(*win.value)); // 绘制当前值

  // 需要在窗口修改参数时立即见效的函数
  if (!strcmp(win.title, "Disp Bri"))
    u8g2_SetContrast(&u8g2, ui.param[DISP_BRI]);

  // 反转屏幕内元素颜色，白天模式遮罩
  u8g2_SetDrawColor(&u8g2, 2);
  if (!ui.param[DARK_MODE])
    u8g2_DrawBox(&u8g2, 0, 0, DISP_W, DISP_H);
}

/************************************* 处理函数 *************************************/

/*********************************** 通用处理函数 ***********************************/

// 列表类页面旋转时判断通用函数
void list_rotate_switch()
{
  if (!list.loop)
  {
    switch (key_msg.id)
    {
    case 1:
      if (ui.select[ui.layer] == 0)
      {
        if (ui.param[LIST_LOOP] && ui.init)
        {
          list.loop = true;
          ui.select[ui.layer] = ui.num[ui.index] - 1;
          if (ui.num[ui.index] > list.line_n)
          {
            list.box_y_trg[ui.layer] = DISP_H - LIST_LINE_H;
            list.y_trg = DISP_H - ui.num[ui.index] * LIST_LINE_H;
          }
          else
            list.box_y_trg[ui.layer] = (ui.num[ui.index] - 1) * LIST_LINE_H;
          break;
        }
        else
          break;
      }
      if (ui.init)
      {
        ui.select[ui.layer] -= 1;
        if (ui.select[ui.layer] < -(list.y_trg / LIST_LINE_H))
        {
          if (!(DISP_H % LIST_LINE_H))
            list.y_trg += LIST_LINE_H;
          else
          {
            if (list.box_y_trg[ui.layer] == DISP_H - LIST_LINE_H * list.line_n)
            {
              list.y_trg += (list.line_n + 1) * LIST_LINE_H - DISP_H;
              list.box_y_trg[ui.layer] = 0;
            }
            else if (list.box_y_trg[ui.layer] == LIST_LINE_H)
            {
              list.box_y_trg[ui.layer] = 0;
            }
            else
              list.y_trg += LIST_LINE_H;
          }
        }
        else
          list.box_y_trg[ui.layer] -= LIST_LINE_H;
        break;
      }

    case 0:
      if (ui.select[ui.layer] == (ui.num[ui.index] - 1))
      {
        if (ui.param[LIST_LOOP] && ui.init)
        {
          list.loop = true;
          ui.select[ui.layer] = 0;
          list.y_trg = 0;
          list.box_y_trg[ui.layer] = 0;
          break;
        }
        else
          break;
      }
      if (ui.init)
      {
        ui.select[ui.layer] += 1;
        if ((ui.select[ui.layer] + 1) > (list.line_n - list.y_trg / LIST_LINE_H))
        {
          if (!(DISP_H % LIST_LINE_H))
            list.y_trg -= LIST_LINE_H;
          else
          {
            if (list.box_y_trg[ui.layer] == LIST_LINE_H * (list.line_n - 1))
            {
              list.y_trg -= (list.line_n + 1) * LIST_LINE_H - DISP_H;
              list.box_y_trg[ui.layer] = DISP_H - LIST_LINE_H;
            }
            else if (list.box_y_trg[ui.layer] == DISP_H - LIST_LINE_H * 2)
            {
              list.box_y_trg[ui.layer] = DISP_H - LIST_LINE_H;
            }
            else
              list.y_trg -= LIST_LINE_H;
          }
        }
        else
          list.box_y_trg[ui.layer] += LIST_LINE_H;
        break;
      }
      break;
    }
  }
}

// 弹窗通用处理函数
void window_proc()
{
  window_show();
  if (win.y == WIN_Y_TRG)
    ui.index = win.index;
  if (key_msg.press && win.y == win.y_trg && win.y != WIN_Y_TRG)
  {
    key_msg.press = false;
    switch (key_msg.id)
    {
    case 0:
      if (*win.value < win.max)
        *win.value += win.step;
       eeprom.change = true;
      break;
    case 1:
      if (*win.value > win.min)
        *win.value -= win.step;
       eeprom.change = true;
      break;
    case 3:
    case 4:
      win.y_trg = WIN_Y_TRG;
      break;
    }
  }
}

/********************************** 分页面处理函数 **********************************/

// 睡眠页面处理函数
void sleep_proc()
{
  while (ui.sleep)
  {
    // 睡眠时循环执行的函数
	
    // 睡眠时需要扫描旋钮才能退出睡眠
    key_scan();

    // 当旋钮有动作时
    if (key_msg.press)
    {
      key_msg.press = false;
      switch (key_msg.id)
      {
      default:
		{
        ui.index = M_MAIN;
        ui.state = S_LAYER_IN;
        u8g2_SetPowerSave(&u8g2, 0);
        ui.sleep = false;
	   
        break;
		}
      }
    }
  }
}

// 主菜单处理函数，无选项框列表类模板
void main_proc()
{
  list_show(main_menu, M_MAIN);
  if (key_msg.press)
  {
    key_msg.press = false;
    switch (key_msg.id)
    {
    case 0:
    case 1:
      list_rotate_switch();
      break;
    case 2:
      ui.select[ui.layer] = 0;
    case 3:
      switch (ui.select[ui.layer])
      {
      case 0:
        ui.index = M_SLEEP;
        ui.state = S_LAYER_OUT;
        break;
      case 1:
        ui.index = M_EDITOR;
        ui.state = S_LAYER_IN;
        break;
      case 2:
        ui.index = M_SETTING;
        ui.state = S_LAYER_IN;
        break;
      }
    }
  }
}

// 编辑器菜单处理函数
void editor_proc()
{
  list_show(editor_menu, M_EDITOR);
  if (key_msg.press)
  {
    key_msg.press = false;
    switch (key_msg.id)
    {
    case 0:
    case 1:
      list_rotate_switch();
      break;
    case 2:
      ui.select[ui.layer] = 0;
    case 3:
      switch (ui.select[ui.layer])
      {

      case 0:
        ui.index = M_MAIN;
        ui.state = S_LAYER_OUT;
        break;
      case 11:
        ui.index = M_KNOB;
        ui.state = S_LAYER_IN;
        break;
      }
    }
  }
}

// 旋钮编辑菜单处理函数
void knob_proc()
{
  list_show(knob_menu, M_KNOB);
  if (key_msg.press)
  {
    key_msg.press = false;
    switch (key_msg.id)
    {
    case 0:
    case 1:
      list_rotate_switch();
      break;
    case 2:
      ui.select[ui.layer] = 0;
    case 3:
      switch (ui.select[ui.layer])
      {

      case 0:
        ui.index = M_EDITOR;
        ui.state = S_LAYER_OUT;
        break;
      case 1:
        ui.index = M_KRF;
        ui.state = S_LAYER_IN;
        check_box_s_init(&knob.param[KNOB_ROT], &knob.param[KNOB_ROT_P]);
        break;
      case 2:
        ui.index = M_KPF;
        ui.state = S_LAYER_IN;
        check_box_s_init(&knob.param[KNOB_COD], &knob.param[KNOB_COD_P]);
        break;
      }
    }
  }
}

// 旋钮旋转功能菜单处理函数
void krf_proc()
{
  list_show(krf_menu, M_KRF);
  if (key_msg.press)
  {
    key_msg.press = false;
    switch (key_msg.id)
    {
    case 0:
    case 1:
      list_rotate_switch();
      break;
    case 2:
      ui.select[ui.layer] = 0;
    case 3:
      switch (ui.select[ui.layer])
      {

      case 0:
        ui.index = M_KNOB;
        ui.state = S_LAYER_OUT;
        break;
      case 1:
        break;
      case 2:
        check_box_s_select(KNOB_DISABLE, ui.select[ui.layer]);
        break;
      case 3:
        break;
      case 4:
        check_box_s_select(KNOB_ROT_VOL, ui.select[ui.layer]);
        break;
      case 5:
        check_box_s_select(KNOB_ROT_BRI, ui.select[ui.layer]);
        break;
      case 6:
        break;
      }
    }
  }
}

// 旋钮点按功能菜单处理函数
void kpf_proc()
{
  list_show(kpf_menu, M_KPF);
  if (key_msg.press)
  {
    key_msg.press = false;
    switch (key_msg.id)
    {
    case 0:
    case 1:
      list_rotate_switch();
      break;
    case 2:
      ui.select[ui.layer] = 0;
    case 3:
      switch (ui.select[ui.layer])
      {

      case 0:
        ui.index = M_KNOB;
        ui.state = S_LAYER_OUT;
        break;
      case 1:
        break;
      case 2:
        check_box_s_select(KNOB_DISABLE, ui.select[ui.layer]);
        break;
      case 3:
        break;
      case 4:
        check_box_s_select('A', ui.select[ui.layer]);
        break;
      case 5:
        check_box_s_select('B', ui.select[ui.layer]);
        break;
      case 6:
        check_box_s_select('C', ui.select[ui.layer]);
        break;
      case 7:
        check_box_s_select('D', ui.select[ui.layer]);
        break;
      case 8:
        check_box_s_select('E', ui.select[ui.layer]);
        break;
      case 9:
        check_box_s_select('F', ui.select[ui.layer]);
        break;
      case 10:
        check_box_s_select('G', ui.select[ui.layer]);
        break;
      case 11:
        check_box_s_select('H', ui.select[ui.layer]);
        break;
      case 12:
        check_box_s_select('I', ui.select[ui.layer]);
        break;
      case 13:
        check_box_s_select('J', ui.select[ui.layer]);
        break;
      case 14:
        check_box_s_select('K', ui.select[ui.layer]);
        break;
      case 15:
        check_box_s_select('L', ui.select[ui.layer]);
        break;
      case 16:
        check_box_s_select('M', ui.select[ui.layer]);
        break;
      case 17:
        check_box_s_select('N', ui.select[ui.layer]);
        break;
      case 18:
        check_box_s_select('O', ui.select[ui.layer]);
        break;
      case 19:
        check_box_s_select('P', ui.select[ui.layer]);
        break;
      case 20:
        check_box_s_select('Q', ui.select[ui.layer]);
        break;
      case 21:
        check_box_s_select('R', ui.select[ui.layer]);
        break;
      case 22:
        check_box_s_select('S', ui.select[ui.layer]);
        break;
      case 23:
        check_box_s_select('T', ui.select[ui.layer]);
        break;
      case 24:
        check_box_s_select('U', ui.select[ui.layer]);
        break;
      case 25:
        check_box_s_select('V', ui.select[ui.layer]);
        break;
      case 26:
        check_box_s_select('W', ui.select[ui.layer]);
        break;
      case 27:
        check_box_s_select('X', ui.select[ui.layer]);
        break;
      case 28:
        check_box_s_select('Y', ui.select[ui.layer]);
        break;
      case 29:
        check_box_s_select('Z', ui.select[ui.layer]);
        break;
      case 30:
        break;
      case 31:
        check_box_s_select('0', ui.select[ui.layer]);
        break;
      case 32:
        check_box_s_select('1', ui.select[ui.layer]);
        break;
      case 33:
        check_box_s_select('2', ui.select[ui.layer]);
        break;
      case 34:
        check_box_s_select('3', ui.select[ui.layer]);
        break;
      case 35:
        check_box_s_select('4', ui.select[ui.layer]);
        break;
      case 36:
        check_box_s_select('5', ui.select[ui.layer]);
        break;
      case 37:
        check_box_s_select('6', ui.select[ui.layer]);
        break;
      case 38:
        check_box_s_select('7', ui.select[ui.layer]);
        break;
      case 39:
        check_box_s_select('8', ui.select[ui.layer]);
        break;
      case 40:
        check_box_s_select('9', ui.select[ui.layer]);
        break;
      case 41:
        break;
      }
    }
  }
}

// 设置菜单处理函数，多选框列表类模板，弹窗模板
void setting_proc()
{
  list_show(setting_menu, M_SETTING);
  if (key_msg.press)
  {
    key_msg.press = false;
    switch (key_msg.id)
    {
    case 0:
    case 1:
      list_rotate_switch();
      break;
    case 2:
      ui.select[ui.layer] = 0;
    case 3:
      switch (ui.select[ui.layer])
      {

      // 返回更浅层级，长按被当作选择这一项，也是执行这一行
      case 0:
        ui.index = M_MAIN;
        ui.state = S_LAYER_OUT;
        break;

      // 弹出窗口，参数初始化：标题，参数名，参数值，最大值，最小值，步长，背景列表名，背景列表标签
      case 1:
        window_value_init("Disp Bri", DISP_BRI, &ui.param[DISP_BRI], 255, 0, 5, setting_menu, M_SETTING);
        break;
      case 2:
        window_value_init("List Ani", LIST_ANI, &ui.param[LIST_ANI], 100, 10, 1, setting_menu, M_SETTING);
        break;
      case 3:
        window_value_init("Win Ani", WIN_ANI, &ui.param[WIN_ANI], 100, 10, 1, setting_menu, M_SETTING);
        break;
      case 4:
        window_value_init("Fade Ani", FADE_ANI, &ui.param[FADE_ANI], 255, 0, 1, setting_menu, M_SETTING);
        break;
      case 5:
        window_value_init("Btn SPT", BTN_SPT, &ui.param[BTN_SPT], 255, 0, 1, setting_menu, M_SETTING);
        break;
      case 6:
        window_value_init("Btn LPT", BTN_LPT, &ui.param[BTN_LPT], 255, 0, 1, setting_menu, M_SETTING);
        break;

      // 多选框
      case 7:
        check_box_m_select(LIST_UFD);
        break;
      case 8:
        check_box_m_select(LIST_LOOP);
        break;
      case 9:
        check_box_m_select(WIN_BOK);
        break;
      case 10:
        check_box_m_select(KNOB_DIR);
        break;
      case 11:
        check_box_m_select(DARK_MODE);
        break;

      // 关于本机
      case 12:
        ui.index = M_ABOUT;
        ui.state = S_LAYER_IN;
        break;
      }
    }
  }
}


// 关于本机页
void about_proc()
{
  list_show(about_menu, M_ABOUT);
  if (key_msg.press)
  {
    key_msg.press = false;
    switch (key_msg.id)
    {
    case 0:
    case 1:
      list_rotate_switch();
      break;
    case 2:
      ui.select[ui.layer] = 0;
    case 3:
      switch (ui.select[ui.layer])
      {

      case 0:
        ui.index = M_SETTING;
        ui.state = S_LAYER_OUT;
        break;
      }
    }
  }
}

// 总的UI进程
void ui_proc()
{
  u8g2_SendBuffer(&u8g2);
  switch (ui.state)
  {
  case S_FADE:
    fade();
    break; // 转场动画
  case S_WINDOW:
    window_param_init();
    break; // 弹窗初始化
  case S_LAYER_IN:
    layer_init_in();
    break; // 层级初始化
  case S_LAYER_OUT:
    layer_init_out();
    break; // 层级初始化

  case S_NONE:
    u8g2_ClearBuffer(&u8g2);
    switch (ui.index) // 直接选择页面
    {
    case M_WINDOW:
      window_proc();
      break;
    case M_SLEEP:
      sleep_proc();
      break;
    case M_MAIN:
      main_proc();
      break;
    case M_EDITOR:
      editor_proc();
      break;
    case M_KNOB:
      knob_proc();
      break;
    case M_KRF:
      krf_proc();
      break;
    case M_KPF:
      kpf_proc();
      break;
    case M_SETTING:
      setting_proc();
      break;
    case M_ABOUT:
      about_proc();
      break;
    }
  }
}

// OLED初始化函数
void oled_init()
{
  u8g2_InitDisplay(&u8g2);
  u8g2_ClearDisplay(&u8g2);
  u8g2_SetPowerSave(&u8g2, 0);
  u8g2_SetContrast(&u8g2, ui.param[DISP_BRI]);
  buf_ptr = u8g2_GetBufferPtr(&u8g2);
  buf_len = 8 * u8g2_GetBufferTileHeight(&u8g2) * u8g2_GetBufferTileWidth(&u8g2);
}

void setup()
{
  eeprom_init();
  // ui_param_init();
  u8g2_Init();
  ui_init();
  oled_init();
  key_init();

  // hid_init();
}

void loop()
{
  key_scan();
  ui_proc();
}
